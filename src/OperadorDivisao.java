/*******************************************************************************
 *
 * Arquivo  : OperadorDivisao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o operador binário identificado pelo caractere '/'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class OperadorDivisao extends OperadorBinario
{
  public OperadorDivisao(Expressao esquerda, Expressao direita)
  {
    super('/', esquerda, direita);
  }

  protected Complexo calcular(Complexo esquerda, Complexo direita)
  {
    return Complexo.divide(esquerda, direita);
  }

  protected Expressao clonar(Expressao esquerda, Expressao direita)
  {
    return new OperadorDivisao(esquerda, direita);
  }

  protected Expressao derivar(Expressao esquerda, Expressao direita)
  {
    Expressao diffEsq  = esquerda.derivar(),
              diffDir  = direita .derivar(),
              expoente = new Numero(new Complexo(2, 0)),
              sub_Esq  = new OperadorMultiplicacao(diffEsq,  direita),
              sub_Dir  = new OperadorMultiplicacao(esquerda, diffDir),
              novaEsq  = new OperadorSubtracao    (sub_Esq,  sub_Dir),
              novaDir  = new OperadorExponenciacao(direita,  expoente);

    return new OperadorDivisao(novaEsq, novaDir);
  }

  protected Expressao simplificar(Expressao esquerda, Expressao direita)
  {
    boolean eEsquerda = (esquerda instanceof Numero);
    Numero  numero    = eEsquerda ? (Numero)esquerda : (Numero)direita;

    if (numero.eZero())
      return new Numero( eEsquerda ? Complexo.ZERO : Complexo.NaN );
    else if (numero.eUm())
      return eEsquerda ? this : esquerda;
    else
      return this;
  }
}
