/*******************************************************************************
 *
 * Arquivo  : SenoTrigonometrico.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função trigonométrica seno identificada na gramática
 *            pelo nome 'sen'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class SenoTrigonometrico extends Funcao
{
  public SenoTrigonometrico(Expressao entrada)
  {
    super("sen", entrada);
  }

  protected Complexo calcular(Complexo entrada)
  {
    return Complexo.sen(entrada);
  }

  protected Expressao clonar(Expressao entrada)
  {
    return new SenoTrigonometrico(entrada);
  }

  protected Expressao derivar(Expressao entrada)
  {
    return new CossenoTrigonometrico(entrada);
  }
}
