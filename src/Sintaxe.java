/*******************************************************************************
 *
 * Arquivo  : Sintaxe.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe responsável por analisar uma frase e convertê-la a uma
 *            árvore sintática.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.colecoes.PilhaEncadeada;
//import fractallotus.fundacao.matematica.Complexo;

public class Sintaxe
{
  // ATRIBUTOS //
  private String                    frase;
  private PilhaEncadeada<String>    operadores;
  private PilhaEncadeada<Expressao> termos;

  // CONSTRUTOR //
  private Sintaxe(String frase)
  {
    this.frase = frase;
    operadores = new PilhaEncadeada<String>();
    termos     = new PilhaEncadeada<Expressao>();
  }

  // ASSISTENTES //
  private Expressao analisar()
  {
    char operador = '\0';
    int  inicio   = 0;

    do
    {
      try
      {
        int    fim   = proximo(inicio);
        String termo = frase.substring(inicio, fim).trim();

        if (!termo.isEmpty())
          empilhar(termo);

        if (fim == frase.length())
          continue;

        operador = operador(frase.charAt(fim), operador, termo);

        if (operador != '(')
          desempilhar("" + operador);
        if (operador != ')')
          operadores.empilhar("" + operador);

        inicio = fim + 1;
      }
      catch (Exception e)
      {
        return null;
      }
    } while (inicio < frase.length());

    return (operadores.total() != 0 ||
            termos    .total() != 1) ? null : termos.desempilhar();
  }

  private int proximo(int inicio)
  {
    for (int i = inicio; i < frase.length(); i++)
      if (Fabrica.eOperador(frase.charAt(i)))
        return i;

    return frase.length();
  }

  private char operador(char atual, char anterior, String termo)
  {
    if (atual != '-' && atual != '+')
      return atual;
    else if (anterior == '(' && termo.equals(""))
      return (atual == '-') ? '!' : '?';
    else
      return atual;
  }

  private void empilhar(String termo) throws IllegalArgumentException
  {
    if (Fabrica.eFuncao(termo))
      operadores.empilhar(termo);
    else if (Fabrica.eNumero(termo))
      termos.empilhar( Fabrica.criarNumero(termo) );
    else if (Fabrica.eVariavel(termo))
      termos.empilhar( Fabrica.criarVariavel() );
    else
      throw new IllegalArgumentException();
  }

  private void desempilhar(String ate)
  {
    while (!operadores.vazio() && comparar(ate, operadores.espiar()) <= 0)
    {
      String    operador = operadores.desempilhar();
      Expressao direita  = termos    .desempilhar();

      if (Fabrica.eFuncao(operador))
        termos.empilhar( Fabrica.criarFuncao(operador, direita) );
      else if (operador.equals("("))
      {
        termos.empilhar(direita); // devolve o que foi retirado
        break;                    // encerra
      }
      else
      {
        Expressao esquerda = null;

        if (Fabrica.eOperador(operador.charAt(0))) // se não for operador,
          esquerda = termos.desempilhar();         // então é operador unário

        termos.empilhar
        (
          Fabrica.criarOperador(operador.charAt(0), esquerda, direita)
        );
      }
    }
  }

  private int comparar(String esquerda, String direita)
  {
    return prioridade(esquerda) - prioridade(direita);
  }

  private static int prioridade(String operador)
  {
    if (Fabrica.eFuncao(operador))
      return 3;
    else
      switch (operador.charAt(0))
      {
        case '+':
        case '-': return 1;
        case '*':
        case '/': return 2;
        case '^': return 3;
        case '?':
        case '!': return 4;
        default : return 0;
      }
  }

  // INTERFACE //
  public static Expressao analisar(String frase) throws IllegalArgumentException
  {
    Sintaxe   sintaxe  = new Sintaxe(String.format("(%s)", frase.toLowerCase()));
    Expressao result   = sintaxe.analisar(),
              anterior = null;

    if (result == null)
      throw new IllegalArgumentException("frase inválida");

    do
    {
      anterior = result;
      result   = anterior.simplificar();
    } while (result != anterior);

    if (result instanceof Numero && ((Numero)result).eNaN())
      throw new IllegalArgumentException("frase inválida");

    return result;
  }

  public static Expressao derivar(Expressao funcao) throws IllegalArgumentException
  {
    if (funcao == null)
      throw new IllegalArgumentException("expressão não pode ser nula");

    Expressao result   = funcao.derivar(),
              anterior = null;

    do
    {
      anterior = result;
      result   = anterior.simplificar();
    } while (result != anterior);

    if (result instanceof Numero)
    {
      Numero numero = (Numero)result;

      if (numero.eZero() || numero.eNaN())
        throw new IllegalArgumentException("expressão não é uma função holomorfa");
    }

    return result;
  }
}
