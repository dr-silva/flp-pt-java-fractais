/*******************************************************************************
 *
 * Arquivo  : Fabrica.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe que se responsabiliza por validar termos da expressão e
 *            instanciar seus repectivos objetos.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class Fabrica
{
  public static boolean eOperador(char operador)
  {
    return (operador == '+') ||
           (operador == '-') ||
           (operador == '*') ||
           (operador == '/') ||
           (operador == '^') ||
           (operador == '(') ||
           (operador == ')');
  }

  public static boolean eFuncao(String funcao)
  {
    return funcao.matches("^((sen|cos)h?|ln)$");
  }

  public static boolean eNumero(String numero)
  {
    return numero.matches("^((e|pi|\\d+(\\.\\d+)?|\\.\\d+)i?|i)$");
  }

  public static boolean eVariavel(String variavel)
  {
    return variavel.equals("x");
  }

  public static Expressao criarOperador(char operador, Expressao esquerda, Expressao direita)
  {
    switch (operador)
    {
      case '?': return new OperadorPositivo     (direita);
      case '!': return new OperadorNegativo     (direita);
      case '+': return new OperadorAdicao       (esquerda, direita);
      case '-': return new OperadorSubtracao    (esquerda, direita);
      case '*': return new OperadorMultiplicacao(esquerda, direita);
      case '/': return new OperadorDivisao      (esquerda, direita);
      case '^': return new OperadorExponenciacao(esquerda, direita);
      default : return null;
    }
  }

  public static Expressao criarFuncao(String funcao, Expressao entrada)
  {
         if (funcao.equals("sen"))  return new SenoTrigonometrico   (entrada);
    else if (funcao.equals("cos"))  return new CossenoTrigonometrico(entrada);
    else if (funcao.equals("senh")) return new SenoHiperbolico      (entrada);
    else if (funcao.equals("cosh")) return new CossenoHiperbolico   (entrada);
    else if (funcao.equals("ln"))   return new LogaritmoNatural     (entrada);
    else                            return null;
  }

  public static Expressao criarNumero(String numero)
  {
    Complexo valor = null;

         if (numero.equals("i"))   valor = new Complexo(0, 1);
    else if (numero.equals("e"))   valor = new Complexo(Math.E, 0);
    else if (numero.equals("ei"))  valor = new Complexo(0, Math.E);
    else if (numero.equals("pi"))  valor = new Complexo(Math.PI, 0);
    else if (numero.equals("pii")) valor = new Complexo(0, Math.PI);
    else if (numero.indexOf("i") > 0)
      valor = new Complexo
      (
        0, Double.parseDouble(numero.substring(0, numero.length() - 1))
      );
    else
      valor = new Complexo(Double.parseDouble(numero), 0);

    return new Numero(valor);
  }

  public static Expressao criarVariavel()
  {
    return new Variavel();
  }
}
