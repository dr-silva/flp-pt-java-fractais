/*******************************************************************************
 *
 * Arquivo  : LogaritmoNatural.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função logaritmo natural identificada na gramática
 *            pelo nome 'ln'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class LogaritmoNatural extends Funcao
{
  public LogaritmoNatural(Expressao entrada)
  {
    super("ln", entrada);
  }

  protected Complexo calcular(Complexo entrada)
  {
    return Complexo.ln(entrada);
  }

  protected Expressao clonar(Expressao entrada)
  {
    return new LogaritmoNatural(entrada);
  }

  protected Expressao derivar(Expressao entrada)
  {
    Expressao um = new Numero(new Complexo(1, 0));

    return new OperadorDivisao(um, entrada);
  }
}
