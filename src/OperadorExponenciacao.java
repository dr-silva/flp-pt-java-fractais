/*******************************************************************************
 *
 * Arquivo  : OperadorExponenciacao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o operador binário identificado pelo caractere '^'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class OperadorExponenciacao extends OperadorBinario
{
  public OperadorExponenciacao(Expressao esquerda, Expressao direita)
  {
    super('^', esquerda, direita);
  }

  protected Complexo calcular(Complexo esquerda, Complexo direita)
  {
    return Complexo.exp(esquerda, direita);
  }

  protected Expressao clonar(Expressao esquerda, Expressao direita)
  {
    return new OperadorExponenciacao(esquerda, direita);
  }

  protected Expressao derivar(Expressao esquerda, Expressao direita)
  {
    Expressao result;

    if (esquerda instanceof Numero)
    {
      Numero    numero  = (Numero)esquerda;
      Expressao novaEsq = new OperadorExponenciacao(esquerda, direita);
                result  = new OperadorMultiplicacao(novaEsq,  direita.derivar());

      if (!numero.eConstEuler())
      {
        Expressao novaDir = new LogaritmoNatural(esquerda.clonar());
                  result  = new OperadorMultiplicacao(result, novaDir);
      }
    }
    else
    {
      Expressao UM       = new Numero               (new Complexo(1, 0)),
                expoente = new OperadorSubtracao    (direita,  UM),
                funcao   = new OperadorExponenciacao(esquerda, expoente),
                novaEsq  = new OperadorMultiplicacao(funcao,   direita.clonar()),
                novaDir  = esquerda.derivar();
                result   = new OperadorMultiplicacao(novaEsq, novaDir);
    }

    return result.simplificar();
  }

  protected Expressao simplificar(Expressao esquerda, Expressao direita)
  {
    return (direita instanceof Numero && ((Numero)direita).eUm()) ? esquerda : this;
  }
}
