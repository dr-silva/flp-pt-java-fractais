/*******************************************************************************
 *
 * Arquivo  : OperadorUnario.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa operações sobre um único operando.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

abstract class OperadorUnario extends Expressao
{
  private char      operador;
  private Expressao operando;

  public OperadorUnario(char operador, Expressao operando)
  {
    this.operador = operador;
    this.operando = operando;
  }

  public Complexo valorar(Complexo x)
  {
    return calcular(operando.valorar(x));
  }

  public String toString()
  {
    return String.format("(%c %s)",  operador, operando);
  }

  Expressao clonar()
  {
    return clonar(operando.clonar());
  }

  Expressao derivar()
  {
    return clonar(operando.derivar());
  }

  Expressao simplificar()
  {
    operando = operando.simplificar();

    if (operando instanceof Numero)
      return new Numero(calcular(operando.valorar(null)));
    else if (operando.getClass().equals(this.getClass()))
      return ((OperadorUnario)operando).operando;
    else
      return simplificar(operando);
  }

  protected abstract Complexo  calcular   (Complexo operando);
  protected abstract Expressao clonar     (Expressao operando);
  protected abstract Expressao simplificar(Expressao operando);
}
