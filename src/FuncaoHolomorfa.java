/*******************************************************************************
 *
 * Arquivo  : FuncaoHolomorfa.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função holomorfa e sua derivada, tem como responsa-
 *            bilidade encontrar a raiz dum ponto.
 *
 ******************************************************************************/
//package fractallotus.fratais;

//import fractallotus.fratais.expressoes.Expressao;
//import fractallotus.fratais.expressoes.Constantes;

import fractallotus.fundacao.matematica.Complexo;

public class FuncaoHolomorfa
{
  private final Expressao funcao,
                          derivada;

  public FuncaoHolomorfa(Expressao funcao, Expressao derivada)
  {
    if (funcao == null)
      throw new IllegalArgumentException("função não pode ser nula");
    if (derivada == null)
      throw new IllegalArgumentException("derivada não pode ser nula");

    this.funcao   = funcao;
    this.derivada = derivada;
  }

  public Raiz obterRaiz(Complexo x)
  {
    int      iteracoes = 0;
    Complexo anterior  = x;

    do
    {
      if (Constantes.eTeto(iteracoes++))
        return new Raiz(Complexo.NaN, 0);

      anterior   = x;
      Complexo f = funcao  .valorar(anterior),
               d = derivada.valorar(anterior);

      if (Complexo.eNaN(f) || Complexo.eNaN(d) || Constantes.eZero(d))
        return new Raiz(Complexo.NaN, 0);

      x = Complexo.subtract(anterior, Complexo.divide(f, d));
    } while (!Constantes.saoIguais(x, anterior));

    return new Raiz(x, iteracoes);
  }

  public void imprimirInfos()
  {
    System.out.println("funcao  : " + funcao);
    System.out.println("derivada: " + derivada);
  }
}
