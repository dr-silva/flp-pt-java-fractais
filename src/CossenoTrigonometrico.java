/*******************************************************************************
 *
 * Arquivo  : CossenoTrigonometrico.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função trigonométrica cosseno identificada na gramá-
 *            tica pelo nome 'cos'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class CossenoTrigonometrico extends Funcao
{
  public CossenoTrigonometrico(Expressao entrada)
  {
    super("cos", entrada);
  }

  protected Complexo calcular(Complexo entrada)
  {
    return Complexo.cos(entrada);
  }

  protected Expressao clonar(Expressao entrada)
  {
    return new CossenoTrigonometrico(entrada);
  }

  protected Expressao derivar(Expressao entrada)
  {
    return new OperadorNegativo(new SenoTrigonometrico(entrada));
  }
}
