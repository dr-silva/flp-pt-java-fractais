/*******************************************************************************
 *
 * Arquivo  : Fractais.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-20 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Executa o programa que gera os fractais.
 *
 ******************************************************************************/
//package fractallotus.fratais;

public class Fractais
{
  public final static void main(String[] args)
  {
    switch (args.length)
    {
      case  0: executar();        break;
      case  1: executar(args[0]); break;
      default: imprimirErro();    break;
    }
  }

  private static void executar()
  {
    Config config = new Config();

    System.out.println("Lendo os dados...");
    config.lerDados();

    if (config.haErros())
      config.imprimirErros();
    else
    {
      System.out.println("Preparando o fractal...");
      BaciaNewton bacia = new BaciaNewton(config);

      bacia.desenhar();
      bacia.salvar();
    }
  }

  private static void executar(String arg)
  {
    String opcao = arg.toLowerCase();

    if (opcao.equals("config"))
      Config.imprimirArquivo();
    else if (opcao.equals("ajuda"))
      imprimirAjuda();
    else
      imprimirErro();
  }

  private static void imprimirAjuda()
  {
    System.out.println("use fractais [ajuda] [config > config.txt] [< config.txt]");
    System.out.println("");
    System.out.println("    ajuda                 imprime as informações de uso");
    System.out.println("    config > config.txt   gera o arquivo de configurações");
    System.out.println("    < config.txt          gera o fractal com base no arquivo de configuração");
  }

  private static void imprimirErro()
  {
    System.out.println("Opções de entrada desconhecidas.");
    System.out.println("Use a opção \"ajuda\" para mais informações.");
  }
}
