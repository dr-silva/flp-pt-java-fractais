/*******************************************************************************
 *
 * Arquivo  : OperadorSubtracao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o operador binário identificado pelo caractere '-'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class OperadorSubtracao extends OperadorBinario
{
  public OperadorSubtracao(Expressao esquerda, Expressao direita)
  {
    super('-', esquerda, direita);
  }

  protected Complexo calcular(Complexo esquerda, Complexo direita)
  {
    return Complexo.subtract(esquerda, direita);
  }

  protected Expressao clonar(Expressao esquerda, Expressao direita)
  {
    return new OperadorSubtracao(esquerda, direita);
  }

  protected Expressao derivar(Expressao esquerda, Expressao direita)
  {
    return new OperadorSubtracao(esquerda.derivar(), direita.derivar());
  }

  protected Expressao simplificar(Expressao esquerda, Expressao direita)
  {
    if (direita instanceof Numero && ((Numero)direita).eZero())
      return esquerda;
    else if (esquerda instanceof Numero && ((Numero)esquerda).eZero())
      return (new OperadorNegativo(direita)).simplificar();
    else
      return this;
  }
}
