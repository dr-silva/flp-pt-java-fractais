/*******************************************************************************
 *
 * Arquivo  : OperadorBinario.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa uma operação sobre dois operandos, um à esquerda e
 *            outro à direita do operador.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

abstract class OperadorBinario extends Expressao
{
  private char      operador;
  private Expressao esquerda,
                    direita;

  public OperadorBinario(char operador, Expressao esquerda, Expressao direita)
  {
    this.operador = operador;
    this.esquerda = esquerda;
    this.direita  = direita;
  }

  public Complexo valorar(Complexo x)
  {
    return calcular(esquerda.valorar(x), direita.valorar(x));
  }

  public String toString()
  {
    return String.format("(%s %c %s)", esquerda, operador, direita);
  }

  Expressao clonar()
  {
    return clonar(esquerda.clonar(), direita.clonar());
  }

  Expressao derivar()
  {
    return derivar(esquerda.clonar(), direita.clonar());
  }

  Expressao simplificar()
  {
    esquerda = esquerda.simplificar();
    direita  = direita .simplificar();

    if (esquerda instanceof Numero && direita instanceof Numero)
      return new Numero
      (
        calcular(esquerda.valorar(null), direita.valorar(null))
      );
    else if (esquerda instanceof Numero || direita instanceof Numero)
      return simplificar(esquerda, direita);
    else
      return this;
  }

  protected abstract Complexo  calcular   (Complexo  esquerda, Complexo  direita);
  protected abstract Expressao clonar     (Expressao esquerda, Expressao direita);
  protected abstract Expressao derivar    (Expressao esquerda, Expressao direita);
  protected abstract Expressao simplificar(Expressao esquerda, Expressao direita);
}
