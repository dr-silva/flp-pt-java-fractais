/*******************************************************************************
 *
 * Arquivo  : CorRaiz.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: 
 *
 ******************************************************************************/
//package fractallotus.fratais;

import java.awt.Color;
import fractallotus.fundacao.matematica.Complexo;

class ParRaizCor
{
  public final Complexo raiz;
  public final Color    cor;

  public ParRaizCor(Complexo raiz, Color cor)
  {
    this.raiz = raiz;
    this.cor  = cor;
  }
}
