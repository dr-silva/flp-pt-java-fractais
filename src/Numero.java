/*******************************************************************************
 *
 * Arquivo  : Numero.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa um número da expressão conforme documentação.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;
//import fractallotus.fratais.Constantes;

class Numero extends Expressao
{
  private final Complexo valor;

  public Numero(Complexo valor)
  {
    this.valor = valor;
  }

  public Complexo valorar(Complexo x)
  {
    return valor;
  }

  public String toString()
  {
    return "(" + valor.toString() + ")";
  }

  public boolean eReal()
  {
    return Constantes.eReal(valor);
  }

  public boolean eImaginario()
  {
    return Constantes.eImaginario(valor);
  }

  public boolean eZero()
  {
    return Constantes.eZero(valor);
  }

  public boolean eUm()
  {
    return Constantes.saoIguais(valor, new Complexo(1, 0));
  }

  public boolean eConstEuler()
  {
    return Constantes.saoIguais(valor, new Complexo(Math.E, 0));
  }

  public boolean eNaN()
  {
    return Complexo.eNaN(valor);
  }

  public Complexo valor()
  {
    return valor;
  }

  Expressao clonar()
  {
    return new Numero(valor);
  }

  Expressao derivar()
  {
    return new Numero(Complexo.ZERO);
  }

  Expressao simplificar()
  {
    return this;
  }
}
