/*******************************************************************************
 *
 * Arquivo  : OperadorAdicao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o operador binário identificado pelo caractere '+'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class OperadorAdicao extends OperadorBinario
{
  public OperadorAdicao(Expressao esquerda, Expressao direita)
  {
    super('+', esquerda, direita);
  }

  protected Complexo calcular(Complexo esquerda, Complexo direita)
  {
    return Complexo.add(esquerda, direita);
  }

  protected Expressao clonar(Expressao esquerda, Expressao direita)
  {
    return new OperadorAdicao(esquerda, direita);
  }

  protected Expressao derivar(Expressao esquerda, Expressao direita)
  {
    return new OperadorAdicao(esquerda.derivar(), direita.derivar());
  }

  protected Expressao simplificar(Expressao esquerda, Expressao direita)
  {
    if (esquerda instanceof Numero && ((Numero)esquerda).eZero())
      return direita;
    else if (direita instanceof Numero && ((Numero)direita).eZero())
      return esquerda;
    else
      return this;
  }
}
