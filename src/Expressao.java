/*******************************************************************************
 *
 * Arquivo  : Expressão.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o nó da árvore sintática das expressões matemáticas.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

public abstract class Expressao
{
  public abstract Complexo valorar(Complexo x);

  abstract Expressao clonar();
  abstract Expressao derivar();
  abstract Expressao simplificar();
}
