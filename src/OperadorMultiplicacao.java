/*******************************************************************************
 *
 * Arquivo  : OperadorMultiplicacao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o operador binário identificado pelo caractere '*'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class OperadorMultiplicacao extends OperadorBinario
{
  public OperadorMultiplicacao(Expressao esquerda, Expressao direita)
  {
    super('*', esquerda, direita);
  }

  protected Complexo calcular(Complexo esquerda, Complexo direita)
  {
    return Complexo.multiply(esquerda, direita);
  }

  protected Expressao clonar(Expressao esquerda, Expressao direita)
  {
    return new OperadorMultiplicacao(esquerda, direita);
  }

  protected Expressao derivar(Expressao esquerda, Expressao direita)
  {
    Expressao diffEsq = esquerda.derivar(),
              diffDir = direita .derivar(),
              novaEsq = new OperadorMultiplicacao(diffEsq,  direita),
              novaDir = new OperadorMultiplicacao(esquerda, diffDir);

    return new OperadorAdicao(novaEsq, novaDir);
  }

  protected Expressao simplificar(Expressao esquerda, Expressao direita)
  {
    boolean   eEsquerda = (esquerda instanceof Numero);
    Numero    numero    = eEsquerda ? (Numero)esquerda : (Numero)direita;
    Expressao expressao = eEsquerda ? direita : esquerda;

    if (numero.eZero())
      return new Numero(Complexo.ZERO);
    else if (numero.eUm())
      return expressao;
    else
      return this;
  }
}
