/*******************************************************************************
 *
 * Arquivo  : OperadorNegativo.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o operador unário identificado pelo caractere '-'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class OperadorNegativo extends OperadorUnario
{
  public OperadorNegativo(Expressao operando)
  {
    super('-', operando);
  }

  protected Complexo calcular(Complexo operando)
  {
    return Complexo.negative(operando);
  }

  protected Expressao clonar(Expressao operando)
  {
    return new OperadorNegativo(operando);
  }

  protected Expressao simplificar(Expressao operando)
  {
    return this;
  }
}
