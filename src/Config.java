/*******************************************************************************
 *
 * Arquivo  : Config.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe que lê e escreve o arquivo de configurações.
 *
 ******************************************************************************/
//package fractallotus.fratais;

import java.util.Scanner;
import java.io.Console;

//import fractallotus.fractais.expressoes.Expressao;
//import fractallotus.fractais.expressoes.Sintaxe;
import fractallotus.fundacao.colecoes.BolsaEncadeada;
import fractallotus.fundacao.entradas_saidas.ImagemBitmap;

class Config
{
  private final BolsaEncadeada<String> fErros;

  private double    fReInicial,
                    fImInicial,
                    fReDimensao,
                    fImDimensao;
  private int       fImgAltura,
                    fImgLargura;
  private long      fSemente;
  private String    fArquivo;
  private Expressao fFuncao,
                    fDerivada;

  // INTERFACE //
  public Config()
  {
    fErros = new BolsaEncadeada<String>();

    fReInicial  = Double.NaN;
    fImInicial  = Double.NaN;
    fReDimensao = Double.NaN;
    fImDimensao = Double.NaN;
    fImgAltura  = Integer.MIN_VALUE;
    fImgLargura = Integer.MIN_VALUE;
    fSemente    = System.currentTimeMillis();
    fArquivo    = null;
    fFuncao     = null;
    fDerivada   = null;
  }

  public void lerDados()
  {
    int available;

    try                 { available = System.in.available(); }
    catch (Exception e) { available = 0;}

    Console console = System.console();

    if (console != null || available == 0)
    {
      fErros.guardar("A entrada do console não foi redirecionada.");
      fErros.guardar("Use a opção \"ajuda\" para mais informações.");
    }
    else
    {
      Scanner leitor = new Scanner(System.in);

      int linha = 1;

      while (leitor.hasNextLine())
        lerDado(leitor.nextLine(), linha++);

      leitor.close();
      lerDerivada();
      validarLeitura();
    }
  }

  public void imprimirErros()
  {
    for (String str : fErros)
      System.out.println(str);
  }

  public static void imprimirArquivo()
  {
    System.out.println("reInicial =0          # deve ser um ponto-flutuante");
    System.out.println("imInicial =0          # deve ser um ponto-flutuante");
    System.out.println("reDimensao=1          # deve ser um ponto-flutuante positivo");
    System.out.println("imDimensao=1          # deve ser um ponto-flutuante positivo");
    System.out.println("imgAltura =100        # deve ser um número inteiro positivo");
    System.out.println("imgLargura=100        # deve ser um número inteiro positivo");
    System.out.println("funcao    =x^4-1      # deve ser uma expressão que represente a função");
    System.out.println("#derivada  =4*x^3      # deve ser uma expressão que represente a derivada");
    System.out.println("#semente   =20211020   # deve ser um número inteiro positivo");
    System.out.println("arquivo   =teste.png  # deve ser um arquivo cuja extensão é PNG");
  }

  // PROPRIEDADES //
  public boolean haErros()
  {
    return !fErros.vazio();
  }

  public double    reInicial()  { return fReInicial;  }
  public double    imInicial()  { return fImInicial;  }
  public double    reDimensao() { return fReDimensao; }
  public double    imDimensao() { return fImDimensao; }
  public int       imgAltura()  { return fImgAltura;  }
  public int       imgLargura() { return fImgLargura; }
  public long      semente()    { return fSemente;    }
  public String    arquivo()    { return fArquivo;    }
  public Expressao funcao()     { return fFuncao;     }
  public Expressao derivada()   { return fDerivada;   }

  // ASSISTENTES //
  private void validarLeitura()
  {
    if (Double.isNaN(fReInicial))
      fErros.guardar("A chave \"reInicial\" não consta no arquivo");
    if (Double.isNaN(fImInicial))
      fErros.guardar("A chave \"imInicial\" não consta no arquivo");
    if (Double.isNaN(fReDimensao))
      fErros.guardar("A chave \"reDimensao\" não consta no arquivo");
    if (Double.isNaN(fImDimensao))
      fErros.guardar("A chave \"imDimensao\" não consta no arquivo");
    if (fImgAltura == Integer.MIN_VALUE)
      fErros.guardar("A chave \"imgAltura\" não consta no arquivo");
    if (fImgLargura == Integer.MIN_VALUE)
      fErros.guardar("A chave \"imgLargura\" não consta no arquivo");
    if (fArquivo == null)
      fErros.guardar("A chave \"arquivo\" não consta no arquivo");
    if (fFuncao == null)
      fErros.guardar("A chave \"funcao\" não consta no arquivo");
  }

  private void lerDado(String str, int linha)
  {
    int comentario = str.indexOf('#');
    str = (comentario >= 0) ? str.substring(0, comentario) : str;
    str = str.trim();

    if (str.isEmpty())
      return;

    String[] par = str.split("=");
    if (par.length != 2)
      fErros.guardar(String.format("A linha %d está fora do padrão chave=valor", linha));
    else
      lerChaveValor(par[0].trim().toLowerCase(), par[1].trim().toLowerCase(), linha);
  }

  private void lerChaveValor(String chave, String valor, int linha)
  {
         if (chave.equals("reinicial"))  lerReInicial (valor, linha);
    else if (chave.equals("iminicial"))  lerImInicial (valor, linha);
    else if (chave.equals("redimensao")) lerReDimensao(valor, linha);
    else if (chave.equals("imdimensao")) lerImDimensao(valor, linha);
    else if (chave.equals("imgaltura"))  lerImgAltura (valor, linha);
    else if (chave.equals("imglargura")) lerImgLargura(valor, linha);
    else if (chave.equals("semente"))    lerSemente   (valor, linha);
    else if (chave.equals("arquivo"))    lerArquivo   (valor, linha);
    else if (chave.equals("funcao"))     lerFuncao    (valor, linha);
    else if (chave.equals("derivada"))   lerDerivada  (valor, linha);
    else
      fErros.guardar(String.format("A chave \"%s\" na linha %d não é válida", chave, linha));
  }

  private void lerReInicial(String valor, int linha)
  {
    try
    {
      fReInicial = Double.parseDouble(valor);
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"reInicial\" (na linha %d) não é um número válido", linha)
      );
    }
  }

  private void lerImInicial(String valor, int linha)
  {
    try
    {
      fImInicial = Double.parseDouble(valor);
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"imInicial\" (na linha %d) não é um número válido", linha)
      );
    }
  }

  private void lerReDimensao(String valor, int linha)
  {
    try
    {
      fReDimensao = Double.parseDouble(valor);
      if (fReDimensao <= 0)
        fErros.guardar
        (
          String.format("O valor da chave \"reDimensao\" (na linha %d) não é um número positivo válido", linha)
        );
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"reDimensao\" (na linha %d) não é um número válido", linha)
      );
    }
  }

  private void lerImDimensao(String valor, int linha)
  {
    try
    {
      fImDimensao = Double.parseDouble(valor);
      if (fImDimensao <= 0)
        fErros.guardar
        (
          String.format("O valor da chave \"imDimensao\" (na linha %d) não é um número positivo válido", linha)
        );
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"imDimensao\" (na linha %d) não é um número válido", linha)
      );
    }
  }

  private void lerImgAltura(String valor, int linha)
  {
    try
    {
      fImgAltura = Integer.parseInt(valor);
      if (fImgAltura <= 0)
        fErros.guardar
        (
          String.format("O valor da chave \"imgAltura\" (na linha %d) não é um inteiro positivo válido", linha)
        );
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"imgAltura\" (na linha %d) não é um inteiro válido", linha)
      );
    }
  }

  private void lerImgLargura(String valor, int linha)
  {
    try
    {
      fImgLargura = Integer.parseInt(valor);
      if (fImgLargura <= 0)
        fErros.guardar
        (
          String.format("O valor da chave \"imgAltura\" (na linha %d) não é um inteiro positivo válido", linha)
        );
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"imgAltura\" (na linha %d) não é um inteiro válido", linha)
      );
    }
  }

  private void lerSemente(String valor, int linha)
  {
    try
    {
      fSemente = Long.parseLong(valor);
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"imgAltura\" (na linha %d) não é um inteiro válido", linha)
      );
    }
  }

  private void lerArquivo(String valor, int linha)
  {
    try
    {
      fArquivo = "";

      if (!valor.endsWith(".png"))
        fErros.guardar
        (
          String.format("O valor da chave \"arquivo\" (na linha %d) não contém a extensão PNG", linha)
        );
      else
      {
        ImagemBitmap imagem = new ImagemBitmap(1, 1);
        imagem.salvar(valor);

        fArquivo = valor;
      }
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"arquivo\" (na linha %d) não é um arquivo válido", linha)
      );
    }
  }

  private void lerFuncao(String valor, int linha)
  {
    try
    {
      fFuncao = Sintaxe.analisar(valor);
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"funcao\" (na linha %d) não é uma expressão válida", linha)
      );
    }
  }

  private void lerDerivada(String valor, int linha)
  {
    try
    {
      fDerivada = Sintaxe.analisar(valor);
    }
    catch (Exception e)
    {
      fErros.guardar
      (
        String.format("O valor da chave \"derivada\" (na linha %d) não é uma expressão válida", linha)
      );
    }
  }

  private void lerDerivada()
  {
    if ((fFuncao != null) && (fDerivada == null))
      try
      {
        fDerivada = Sintaxe.derivar(fFuncao);
      }
      catch (Exception e)
      {
        fErros.guardar
        (
          String.format("A função \"%s\" não é uma função holomorfa", fFuncao)
        );
      }
  }
}
