/*******************************************************************************
 *
 * Arquivo  : SenoHiperbolico.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função hiperbólica seno identificada na gramática
 *            pelo nome 'senh'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class SenoHiperbolico extends Funcao
{
  public SenoHiperbolico(Expressao entrada)
  {
    super("senh", entrada);
  }

  protected Complexo calcular(Complexo entrada)
  {
    return Complexo.senh(entrada);
  }

  protected Expressao clonar(Expressao entrada)
  {
    return new SenoHiperbolico(entrada);
  }

  protected Expressao derivar(Expressao entrada)
  {
    return new CossenoHiperbolico(entrada);
  }
}
