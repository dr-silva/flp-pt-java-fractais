/*******************************************************************************
 *
 * Arquivo  : Variavel.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa uma variável na expressão, identificada pelo caractere
 *            'x'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class Variavel extends Expressao
{
  public Complexo valorar(Complexo x)
  {
    return x;
  }

  public String toString()
  {
    return "x";
  }

  Expressao clonar()
  {
    return new Variavel();
  }

  Expressao derivar()
  {
    return new Numero(new Complexo(1, 0));
  }

  Expressao simplificar()
  {
    return this;
  }
}
