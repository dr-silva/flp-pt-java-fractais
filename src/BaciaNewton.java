/*******************************************************************************
 *
 * Arquivo  : Raiz.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a raiz de uma função holomorfa.
 *            Classe necessária (como uma gambiarra) porque o Java não aceita
 *            parâmetros de saída.
 *
 ******************************************************************************/
//package fractallotus.fratais;

import java.awt.Color;

//import fractallotus.fractais.expressoes.Constantes;

import fractallotus.fundacao.cerne.Periodo;
import fractallotus.fundacao.cerne.Temporizador;
import fractallotus.fundacao.colecoes.BolsaEncadeada;
import fractallotus.fundacao.entradas_saidas.ImagemBitmap;
import fractallotus.fundacao.matematica.Complexo;
import fractallotus.fundacao.matematica.IGeradorCongruencial;
import fractallotus.fundacao.matematica.TipoGeradorCongruencial;
import fractallotus.fundacao.matematica.FabricaGeradorCongruencial;

class BaciaNewton
{
  // ATRIBUTOS //
  private Complexo                   inicio,
                                     dimensao;
  private String                     arquivo;
  private Periodo                    tempo;
  private ImagemBitmap               fractal;
  private FuncaoHolomorfa            funcao;
  private IGeradorCongruencial       aleatorio;
  private BolsaEncadeada<ParRaizCor> raizes;

  // CONSTRUTOR //
  public BaciaNewton(Config config)
  {
    arquivo   = config.arquivo();
    inicio    = new Complexo       (config.reInicial(),  config.imInicial());
    dimensao  = new Complexo       (config.reDimensao(), config.imDimensao());
    fractal   = new ImagemBitmap   (config.imgAltura(),  config.imgLargura());
    funcao    = new FuncaoHolomorfa(config.funcao(),     config.derivada());
    raizes    = new BolsaEncadeada<ParRaizCor>();
    aleatorio = FabricaGeradorCongruencial.criar
    (
      "posix", TipoGeradorCongruencial.Linear, config.semente()
    );

    fractal.originarDoInferiorEsquerdo();
  }

  // ASSISTENTES //
  private Complexo calcularPonto(int linha, int coluna)
  {
    return new Complexo
    (
      inicio.re() + dimensao.re() * (double)coluna / (double)(fractal.largura() - 1),
      inicio.im() + dimensao.im() * (double)linha  / (double)(fractal.altura () - 1)
    );
  }

  private Color obterCor(Raiz x)
  {
    if (Complexo.eNaN(x.raiz))
      return Color.BLACK;

    Color cor = null;

    for (ParRaizCor item : raizes)
      if (Constantes.saoIguais(x.raiz, item.raiz))
      {
        cor = item.cor;
        break;
      }

    if (cor == null)
    {
      cor = new Color(aleatorio.uniforme(0xFFFFFF));

      raizes.guardar(new ParRaizCor(x.raiz, cor));
    }

    double porcentagem = Constantes.porcentagem(x.iteracoes);
    return new Color
    (
      (int)(cor.getRed()   * porcentagem),
      (int)(cor.getGreen() * porcentagem),
      (int)(cor.getBlue()  * porcentagem)
    );
  }

  // INTERFACE //
  public void desenhar()
  {
    Temporizador temporizador = new Temporizador();

    for (int linha = 0; linha < fractal.altura(); linha++)
      for (int coluna = 0; coluna < fractal.largura(); coluna++)
      {
        Complexo ponto = calcularPonto(linha, coluna);
        Raiz     raiz  = funcao.obterRaiz(ponto);

        fractal.pixel(linha, coluna, obterCor(raiz));
      }

    tempo = temporizador.decorrer();
  }

  public void salvar()
  {
    try
    {
      fractal.salvar(arquivo);

      // imprimir dados
      funcao.imprimirInfos();
      System.out.println("semente : " + aleatorio.semente());
      System.out.println("arquivo : " + arquivo);
      System.out.println("tempo   : " + tempo);
    }
    catch (Exception e)
    {
      // faça nada - esse erro não vai acontecer
    }
  }
}
