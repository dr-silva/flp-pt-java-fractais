/*******************************************************************************
 *
 * Arquivo  : Raiz.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a raiz de uma função holomorfa.
 *            Classe necessária (como uma gambiarra) porque o Java não aceita
 *            parâmetros de saída.
 *
 ******************************************************************************/
//package fractallotus.fratais;

import fractallotus.fundacao.matematica.Complexo;

class Raiz
{
  public final Complexo raiz;
  public final int      iteracoes;

  public Raiz(Complexo raiz, int iteracoes)
  {
    this.raiz      = raiz;
    this.iteracoes = iteracoes;
  }
}
