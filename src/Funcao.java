/*******************************************************************************
 *
 * Arquivo  : Funcao.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa funções nas expressões matemáticas.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

abstract class Funcao extends Expressao
{
  private String    nome;
  private Expressao entrada;

  public Funcao(String nome, Expressao entrada)
  {
    this.nome    = nome;
    this.entrada = entrada;
  }

  public Complexo valorar(Complexo x)
  {
    return calcular(entrada.valorar(x));
  }

  public String toString()
  {
    return String.format("%s(%s)",  nome, entrada);
  }

  Expressao clonar()
  {
    return clonar(entrada.clonar());
  }

  Expressao derivar()
  {
    Expressao derivada = derivar(entrada.clonar());

    if (entrada instanceof Variavel)
      return derivada;
    else
      return new OperadorMultiplicacao(derivada, entrada.derivar());
  }

  Expressao simplificar()
  {
    entrada = entrada.simplificar();

    if (entrada instanceof Numero)
      return new Numero(calcular(entrada.valorar(null)));
    else
      return this;
  }

  protected abstract Complexo  calcular(Complexo  entrada);
  protected abstract Expressao clonar  (Expressao entrada);
  protected abstract Expressao derivar (Expressao entrada);
}
