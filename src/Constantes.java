/*******************************************************************************
 *
 * Arquivo  : Constantes.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de assistência ao tipo Complexo.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

public class Constantes
{
  private static final double EPSILON   = 1e-9;
  private static final int    ITERACOES = 40;

  public static boolean eTeto(int valor)
  {
    return valor > ITERACOES;
  }

  public static double porcentagem(int valor)
  {
    double minimo = eTeto(valor) ? ITERACOES : valor;

    return 1 - (minimo / ITERACOES);
  }

  public static boolean eZero(Complexo valor)
  {
    return Complexo.eZero(valor, EPSILON);
  }

  public static boolean eReal(Complexo valor)
  {
    return Math.abs(valor.im()) < EPSILON;
  }

  public static boolean eImaginario(Complexo valor)
  {
    return Math.abs(valor.re()) < EPSILON;
  }

  public static boolean saoIguais(Complexo esquerda, Complexo direita)
  {
    return Complexo.subtract(esquerda, direita).modulo() < EPSILON;
  }
}
