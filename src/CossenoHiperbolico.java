/*******************************************************************************
 *
 * Arquivo  : CossenoHiperbolico.java
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-11 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função hiperbólica cosseno identificada na gramática
 *            pelo nome 'cosh'.
 *
 ******************************************************************************/
//package fractallotus.fratais.expressoes;

import fractallotus.fundacao.matematica.Complexo;

class CossenoHiperbolico extends Funcao
{
  public CossenoHiperbolico(Expressao entrada)
  {
    super("cosh", entrada);
  }

  protected Complexo calcular(Complexo entrada)
  {
    return Complexo.cosh(entrada);
  }

  protected Expressao clonar(Expressao entrada)
  {
    return new CossenoHiperbolico(entrada);
  }

  protected Expressao derivar(Expressao entrada)
  {
    return new SenoHiperbolico(entrada);
  }
}
